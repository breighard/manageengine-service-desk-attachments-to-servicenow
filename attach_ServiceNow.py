#!/usr/bin/env python3

##############################
# Brad Reighard
# 03/13/19
#   #Versions:
    #0.1 - Default functionality
    #0.2 - Added advanced logging and args
    #0.3 - MimeTypes and Headers added!
    #0.4 - Fixed the URI lookup from a ticket number to a URI
    #1.0 - Move to Object Oriented Approach
    #1.1 - added progress bar to upload section also added ability to write out csv file.
    #1.2 - added progress bar for finding the ticket URI
##############################

#################
# Imports
#################
import json
import os
import sys
import argparse
import csv
from tqdm import tqdm
import time
#custom modules
from snowAttach import snowAttach
import key  ## all URL, userName and Password information goes in the key file.

##############
# VARS
##############
VERSION = "1.2"
#basepath for File Attachments
basepath = './fileAttachments/Request'
count = 0
attachment = []
successfullUploads = 0
failedUploads = 0
#################
# Functions
#################

def get_args():
    """Get arguments from system

    """
    parser = argparse.ArgumentParser(description='Upload all attachemnt files to ServiceNow')
    parser.add_argument("-t", "--test", help="Run, but do not upload any files.",default=False,action="store_true")
    parser.add_argument("-c", "--csv", help="output a csv file",action="store_true",default=False)
    localArgs=parser.parse_args()

    return localArgs
   
#################
# Main Program
#################
if __name__ == '__main__':    
    #Get system arguments
    args = get_args()
    
    #################
    # Loop through directory, and create upload objects
    #################
    directoryName1 = os.listdir(basepath)
    for name1 in directoryName1:
        path1 = (basepath + '/' + name1)
        directory2 = os.listdir(path1)
        for name2 in directory2:
            path2 = (path1 + '/' + name2)
            serviceDeskFileNames = os.listdir(path2)
            for serviceDeskFileName in serviceDeskFileNames:
                serviceDeskTicketNumber=name2  
                fileDirectoryPath= (path2 + '/')
                attachment.append ( snowAttach( serviceDeskFileName, fileDirectoryPath, serviceDeskTicketNumber, key.snowUser, key.snowUserPwd, key.snowUrl))
                count += 1
    #################
    # Do a search and lookup the URI for the ticket
    #################
    x=0
    progress = tqdm(total=count)
    while x < count:
        progress.set_description_str("URI Lookup: " + attachment[x].snowNumber)
        progress.update(1)
        attachment[x].uriLookup()
        x += 1

    #################
    # upload attachments to ServiceNow
    #################
    x=0
    if args.test == True:
        progress = tqdm(total=count)
        while x < count:
            time.sleep(.1)
            progress.set_description_str("test upload: " + attachment[x].snowNumber)
            progress.update(1)
            x += 1
    else:
        progress = tqdm(total=count)
        while x < count:
            if (attachment[x].found == True):
                progress.set_description_str("Upload: " + attachment[x].snowNumber)
                progress.update (1)
                attachment[x].upload()
            else:
                progress.update (1)
            if (attachment[x].uploadSuccess == True ):
                successfullUploads += 1
            else:
                failedUploads += 1
            x += 1

    #################
    # Create a report on each file attached and any errors!
    #################
    print ( "Files Found: ", count, " Successfull Uploads: ", successfullUploads, " Failed Uploads: ", failedUploads  )
    if args.csv == True:
        print ( "Building CSV file uploadedFiles.csv")
        with open( 'uploadedFiles.csv', mode='w') as filesFound:
            filesFoundWriter = csv.writer(filesFound, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            filesFoundWriter.writerow(['Upload Number', 'FileName', 'Path', 'Service Desk Number', 'ServiceNow ticket Number', 'sys_id', 'Ticket Found?', 'Successfull Upload?'])
            x=0
            while x < count:
                filesFoundWriter.writerow([x, attachment[x].name, attachment[x].dir, attachment[x].oldTicketNumber, attachment[x].snowNumber, attachment[x].sysId, attachment[x].found, attachment[x].uploadSuccess ])
                x += 1
    print ("Program run complete")

   
    
        



 