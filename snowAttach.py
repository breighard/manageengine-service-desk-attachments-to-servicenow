# Brad Reighard

# /api/now/attachment/upload 

import mimetypes
import requests

class snowAttach:
    '''
    
    '''

    def __init__(self, fileName, dirPath, ticketNumber, user, userPass, snowUrl):
        self.dir = dirPath
        self.name = fileName
        self.oldTicketNumber = ticketNumber
        self.snowNumber = ( "INC" + ticketNumber.zfill(7) )
        self.type = mimetypes.guess_type( fileName, strict = True)
        self.url = snowUrl
        self.user = user
        self.userPass = userPass
        self.header = {"Accept":"*/*"}
        self.uploadSuccess = False
        
    def uriLookup (self):
        lookupRequestUrl = self.url + '/api/now/table/incident?sysparm_query=number=' + self.snowNumber
        try:
            requestResponse = requests.get(lookupRequestUrl , auth = (self.user, self.userPass ) )
        except:
            self.sysId = '0'
            self.found = False
            self.uploadSuccess = False
        try:
            self.sysId = requestResponse.json()['result'][0]['sys_id']
            self.found = True
        except:
            self.sysId = '0'
            self.found = False
            self.uploadSuccess = False
    
    def upload (self):
        if (self.found == True):
            self.uploadUrl = self.url + '/api/now/attachment/upload'
            tableData = {'table_name':'incident', 'table_sys_id':self.sysId }            
            systemAttachment = {'file': ( self.name , open( self.dir + self.name, 'rb'), self.type, {'Expires': '0'})}
            try:
                uploadResponse = requests.post( self.uploadUrl, auth = ( self.user, self.userPass), headers = self.header, files = systemAttachment, data = tableData)
                if uploadResponse.status_code == 201:
                    self.uploadSuccess = True
                else:
                    self.uploadSuccess = False
            except:
                self.uploadSuccess = False