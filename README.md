Readme:
This python script will take files from a service desk instance and upload them to ServiceNow.

You will need to edit a few things, specifically the user, password, and URL in the key file.  Also, the class uses the assumption you are using INC files with 7 digit pad.  

If you need to use this to attach files from another ticket system to ServiceNow, it shouldn't be too difficult to modify for your needs.  You should be able to use the class snowAttach.py, to use for any ticketing system. 
